**Minix10**

This weeks minix consists of an individual flowchart and 2 flowcharts made in collaboration with my group (Group 11: Mie, Frederikke B, Frederikke S and Cecilie K)

**INDIVIDUAL FLOWCHART:**

![](flowchart3.png)

For my individual flowchart I have chosen my minix7, which was about games. I made a coronavirus game, where the monster has to catch the virus. When making the flowchart I wanted to make it really simple and understandable, which is why the steps are so simple and written in an understandable language. 

**GROUP FLOWCHARTS:**

**FLOWCHART 1: “Identification friend”**

![](flowchart1.png)

**FLOWCHART 2: “Identification game”**

![](flowchart2.png)

**Introduction**

The flowcharts above illustrate how we become critical of how one can identify with almost everything and anything, however at the same time we’d also like to present the opportunity to implement these opportunities in our program. We have a vision about making it available for everyone and also how everyone should feel welcome and accepted.   

Furthermore, it is problematic that we present these opportunities because we somehow still force the user to make a decision of what they identify as. This is a topic which we both find very exciting and intriguing, but also a bit incomprehensible. We have chosen to be critical of the way one can choose to identify themselves as “dead objects”, since it biologically is impossible, although at the same time are we interested in and open to learning more and educating ourselves on this type of outlook on life.

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

You will lose some of the underlying complex logic but the overall flow and understanding of the program is presented. It is difficult to separate the 'friction' from the essentials. 	
It is difficult to choose the focus on the flowchart. What are we trying to illustrate and who are the 	receiver of the flowchart. It is all parameters that need to be considered to maintain a high level of complexity in the algorithmic procedural level.  		

**What are the technical challenges facing the two ideas and how are you going to address these?**

**Flowchart 1:** We have to create a lot of options for what people identify themselves as. This can be a technical problem and take a lot of time to make. By choosing this topic we are entering a sensitive area, because we easily can offend people by talking about this exact topic and by forcing them to choose. 
Also, we are forcing people to make a choice about what they identify themselves as, even though we are trying to understand them by making these options available. 

**Flowchart 2:** Again, we have to make various options, in order to make it possible for people to find something to identify as. This game has some of the same critical issues as “Identification friend”, as it still puts people in a box - even though there are more options in this game, there are only five options to identify with instead of the 50+ different genders one can identify with.    
 							
**In which ways are the individual and the group flowcharts you produced useful?**

The individual flowchart gives an accurate idea of how the program works. It's more technical and explains some technical logic of the program. The cons of building the flowchart in this way makes it unreadable for people who don't understand code. The flowcharts from the group exercise are more general explaining and give an idea of how the program should be developed. It can’t be read by everyone. It will help us as a group to stay on track and have a clear vision for the code that needs to be developed. 









