[Link to my work](http://frederikkesimonsen.gitlab.io/aesthetic-programming/minix)

![](class_01.png)

I took most of my inspiration from the p5.js library (https://p5js.org/reference/), and after that I just played around with the different functions.


**What have I produced?**

I started off playing around with different colors and backgrounds, because that was something I thought was a bit difficult when looking at the different codes and names for each of the colors. I figured I would use some fun and bright colors to make it more interesting to look at, so I came up with this page where the circles follow your mouse around. When you just move the mouse around, the color of the circles is yellow. When you start pressing the mouse the color will turn pink, and you are now able to draw and make whatever your heart desires.

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

My first ever independent coding experience was full of all kinds of emotions. It kind of reminds me of learning a completely new language, where math is involved as well. Starting off with absolutely no experience can be both rewarding and very frustrating at the same time, but I’m looking forward to learning more, and experiencing this whole world of coding, which is very new to me.

**How is the coding process different from, or similar to, reading and writing text?**

I feel like the coding process is very different, yet has some similarities to reading and writing. I think this perspective is biased though, because I am fluent in reading and writing, but completely new to coding and everything that it comes with. 


**What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**

The feeling of being able to create something is something I really enjoy, so I can already tell that coding will be challenging, but hopefully very fun to explore. 

