**MINIX5**

[Link to my work](http://frederikkesimonsen.gitlab.io/aesthetic-programming/minix5)

[Code repository](https://gitlab.com/Frederikkesimonsen/aesthetic-programming/-/blob/master/minix5/sketch.js)

![](class_05.png)

So this week I’ve made a black background with white lines and random colorful ellipses all over the canvas. I decided to make the background black to make the circles and the colors stand out. I wanted to keep the lines in the program to make it more of a pattern, and something for the circles to follow, to make it more coherent. 

**What are the rules in your generative program? Describe how your program
performs over time? How do the rules produce emergent behavior?**

I chose the random function to make the computer choose whenever and where to place the ellipses, and also to make it choose what colors the ellipses should be. I therefore used ‘if’ and ‘else’ as well, to make the computer draw these circles whenever the given conditional statement is true. It’s fascination to look at, because whenever you refresh the page, a new pattern will come up on the screen, which gives it almost endless options. 

**What role do rules and processes have in your work?**

The if-statement used in my program makes it look like the program stops running when there is no more space on the canvas, which means that you have to refresh it, for you to see it running on the canvas again. This could be changed by making it into a for-loop, which is something I probably should have thought of using instead of the if-statement. 

**Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via
rules)? Do you have any further thoughts on the theme of this chapter?**

I got a lot of inspiration from the 10 PRINT program and the Langton’s Ant that we read about in the textbook, but I wanted to change it up, to make the project into my own with my thoughts on how to make this type of project. The assigned readings has also helped me to think more critical about randomness, and that everything might not be as random as we think it is. Especially when it comes to programs and things like these, because even though it seems random, the computer will still have some kind of random patterns and orders to put these in. 

**References:**

I took most of my inspiration from this video by Daniel Shiffman: https://www.youtube.com/watch?v=bEyTZ5ZZxZs, as well as the book: Soon, Winnie. Cox, Geoff. "A Handbook of Software Studies". 2020. p. 123-141.
