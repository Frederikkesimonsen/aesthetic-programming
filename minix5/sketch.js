let x = 0;
let y = 0;
let spacing = 20;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

  function draw() {
    stroke(255);
    if (random(1) < 0.05) {
    ellipse(x,y,spacing,spacing);
    fill(random(255), 0, random(255));
      } else {
    line(x, y+spacing, x+spacing, y);
  }
  x = x + spacing;
  if (x > width) {
    x = 0;
    y = y + spacing;
  }

}
