let s= "?"

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(8);
}

function draw() {
background(69, 79, 161,80);
  drawElements();
  //text
  textSize(150);
  noStroke();
  fill(116, 201, 232);
  text(s,300,300);

}

function drawElements() {
  let num=9;
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  //light blue ellipses
  fill (116, 201, 232);
  ellipse(135,0,32,32);
  //small dark blue ellipses
  fill(53, 60, 117);
  ellipse(35,0,22,22);
  //small white dots in the middle of throbber
  fill(255,255,255);
  ellipse(35,0,12,12);
  //big white ellipses
  fill(255,255,255);
  ellipse(160,0,42,42)
  //white lines
  stroke(255, 255, 255);
  line(60,0,60,height);
}

function windowResized() {
  resizeCanvas(windowWidth,windowHeight);
}
