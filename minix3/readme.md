[Link to my work](http://frederikkesimonsen.gitlab.io/aesthetic-programming/minix3)

[Link to code repository](https://gitlab.com/Frederikkesimonsen/aesthetic-programming/-/blob/master/minix3/sketch.js)

![](class_03.png)

**What do you want to explore and/or express?**

So the very obvious thing I wanted to explore was the new syntaxes that was introduced this week. I found it a bit difficult to go from the previous weeks to something that’s now moving and changing. 

I normally experience throbbers as being a bit stressful at times, because most of the them never tell you how long to wait for. So my inspiration for this week was the feeling you get when looking at most throbbers. For my own perspective, this feeling consists of confusion and frustration, so I wanted to make something that represents this. I started out choosing bright and very dominant colors, but after seeing the result I thought it was a bit too much, so I changed it up to something a bit more coherent.

**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**

So the main time-related syntax I have used is the rotate function, which are those who make the ellipses, lines and question marks go in circles.

**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**

Instagram has a throbber when you’re refreshing the homepage. This can be seen as both exciting and stressful at the same time. The throbber is probably meant to get you excited for whatever pictures you might see next, but personally I find it a bit stressful at times. When it’s loading you won’t be able to tell when the pictures are ready for you to be seen, and even though the waiting time isn’t long, it’s still enough time to make you think about the fact that you have to wait to be able to see the new picture on your homepage. Some people might find it exciting though, because it builds up some sort of excitement, which makes you more curious to see the pictures. So this week has taught me that throbbers are not always meant to be something that should irritate you or make you frustrated. They are there for a good reason ;-) 
