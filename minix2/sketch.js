let t= "Press mouse to change my mood"

let moving_size = 20;

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background('lightblue');
  ellipse(mouseX, mouseY, moving_size, moving_size);

//text
  fill('white')
  strokeWeight(3)
  textSize(40);
  text(t,10,400)

  if(mouseIsPressed){
    fill('lightslategray');
      ellipse(420,150,230,260);
      strokeWeight(4.5)
      fill('white');
      ellipse(375,120,30,30);
      ellipse(475,120,30,30);
      line(390,190,460,190);

  } else{
    fill('lightslategray');
    ellipse(120,150,230,260);
    fill('white');
    ellipse(80,120,30,30);
    ellipse(160,120,30,30);
    arc(120,200,60,60,100, PI);
  }
}
