[Link to my work](http://frederikkesimonsen.gitlab.io/aesthetic-programming/minix2)

Code repository: https://gitlab.com/Frederikkesimonsen/aesthetic-programming/-/blob/master/minix2/sketch.js 

![](class_02.png)
![](class_002.png)


**Describe your program and what you have used and learnt.**

I had a few errors that took me forever to fix, so I used most of my time to play around with different features and shapes, especially if(mouseIsPressed). I wanted to make 2 different emojis with different facial expressions, so I decided to make one that looks happy and one that looks pretty sad/unbothered. I decided to go with the color gray, because that’s a color I find pretty neutral. I was debating whether to go with the standard yellow emoji color, but decided to try something different and go with the light gray one. I have learnt more about the different geometrical shapes and how they combined can become something creative and fun.

**How would you put your emoji into a wider social and cultural context that
concerns a politics of representation, identity, race, colonialism, and so on? (Try
to think through the assigned reading and your coding process, and then expand
that to your own experience and thoughts - this is a difficult task, you may need
to spend some time thinking about it)**

By making the colors on the emojis pretty neutral with only a few facial features it both includes and excludes people. It’s so basic that it’s missing all the little human features like eyebrows, ears and so on, but that’s what makes it including as well, because it’s so simple that pretty much no one can feel excluded. So I think my conclusion with this work is that it will never be 100% neutral no matter what you do, because there will always be some sort of thought or exclusion going on. I find it very fascination how different shapes and sizes can suddenly change the whole facial expression of the emoji, and that’s what I used most of my time doing. Just changing small things here and there. So my work turned out pretty basic, but for me it was a victory to just come up with something myself and make it work, because I still find coding a bit difficult and pretty frustrating as well. However it’s a really rewarding feeling when it finally works out.
