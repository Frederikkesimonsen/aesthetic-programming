**MINIX4**

[Link to my work](http://frederikkesimonsen.gitlab.io/aesthetic-programming/minix4)

[Code repository](https://gitlab.com/Frederikkesimonsen/aesthetic-programming/-/blob/master/minix4/sketch.js)

![](class_04.png)
![](class_004.png)


**THE MORE YOU KNOW**

Nowadays we blindly trust the internet with almost all our information. We don’t think twice about reading the conditions when we put our information out there, but at the same time we are definitely aware that this information is floating around on the internet. This is the reason for the name of this work, which is called 'The More You Know'. Even though we know that our private information shouldn’t be accessible for each and every one, we don’t hesitate when it comes to buying things online, accepting cookies and so on. The work is based around this, because if you were told to write your deepest, darkest secrets online, you probably wouldn’t, because we think that it’s none of other peoples business, but yet we still put our private information out there and trust the internet to keep it safe. 


I didn’t have much time this week so I took most of my inspiration from this p5.js example: https://editor.p5js.org/p5/sketches/Dom:_Input_Button. I did some changes to add a new perspective. The main thing I changed was the text, because I wanted to make something about secrets to relate it to how people normally put their private information out there without hesitation. I also did some other small changes. I added a black background to make the colors pop and make it a bit more interesting to look at, and also a bit mysterious at the same time. I have also changed the position of the text and button because I wanted it to be centered. I have learnt how to insert buttons that are interactive, and make the page more exciting to play around with, which I think is pretty cool, because it gives you new possibilities when it comes to coding.

