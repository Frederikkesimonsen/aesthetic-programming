/*
 * @name Input and Button
 * @description Input text and click the button to see it affect the the canvas.
 */
let input, button, greeting;

function setup() {
  // create canvas
  createCanvas(windowWidth,windowHeight);
  background(51);

  input = createInput();
  input.position(550, 65);

  button = createButton('submit');
  button.position(input.x + input.width, 65);
  button.mousePressed(greet);

  greeting = createElement('h2', 'Tell me a secret!');
  greeting.position(550, 5);

  textAlign(CENTER);
  textSize(12);
}

function greet() {
  const name = input.value();
  greeting.html('Good to know ' + ';)');
  input.value('');

  for (let i = 0; i < 200; i++) {
    push();
    fill(random(255), 105, 155);
    translate(random(width), random(height));
    rotate(random(2 * PI));
    text(name, 0, 0);
    pop();
  }
}
