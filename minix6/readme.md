**MINIX6**

[Link to my work](http://frederikkesimonsen.gitlab.io/aesthetic-programming/minix6)

[Code repository](https://gitlab.com/Frederikkesimonsen/aesthetic-programming/-/blob/master/minix6/sketch.js)

**Before:**
![](class_006.png)

**After:**
![](class_06.png)

**Which MiniX do you plan to rework?**

I have chosen to rework my very first miniX, because I thought it was a fun idea to look back at what I started off with. I wanted to change a few things, while still keeping it pretty simple, so it would still be possible to see the linkage between the two different works. 

**What have you changed and why?**

I first of all started changing the size of the canvas so it filled out the whole screen, because I wanted more space to work with. I remember when I made my very first miniX I wanted to make something where it was possible to draw something, so that it would work somehow like a piece of paper where you draw with your pen or pencil. Back then I didn’t really know how to finish this thought, so that’s why I now have chosen to complete the thought I had back then. After changing the size of the canvas I inserted at text to make it more interesting, as well as adding something new to this miniX. I also changed up the colors, so for example it’s now possible to ‘erase’ whatever you have drawn by pressing the mouse. 

**How would you demonstrate aesthetic programming in your work?**

I really wanted to make my work more aesthetically pleasing to look at, by for example changing up the colors to make it less random and confusing. I also added the windowResized so the canvas resizes whenever the screen does, to again make it more aesthetically pleasing. I didn’t really think about things like these when making minix1, but I think as the weeks go by it’s definitely easier to focus on the more aesthetic things of your program, rather than just making something random. I clearly remember how I didn’t know how to remove the strokes of the outlines of the ellipses, but this time I made sure that there was no stroke, to make it visually better.

**What does it mean by programming as a practice, or even as a method for design?**

I think we are all now starting to realize how powerful programming can be, especially as a method for designing whatever you want. I think some people see programming as a skill that’s only used for whenever you need to build up something like computers and so on from scratch, but it can be used as far more than that. We even saw in our minix2 how coding can be used as an expression of something, like we made emojis to show whatever we wanted to. So it can definitely be used as a statement for whatever you’re trying to communicate.


**What is the relation between programming and digital culture?**

Again, I think this really showed in our minix2 programs, where we designed emojis. Emojis are just one of many things in digital culture, that are used to express feelings. Programming is just another way of being able to communicate something specific, while expressing yourself.

**Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?**

In the text they mention how important it is to include reflective thinking, because that is a big part of the process. This is something I have been thinking a lot about while making my different miniX programs, because you usually have to put a lot of thought into your code to come up with something that makes sense, instead of just creating something completely random. 

**References:**

**Book:** Aesthetic Programming. "A handbook of Software Studies" Soon, Winnie. Cox, Geoff.

**Text:** Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28.

