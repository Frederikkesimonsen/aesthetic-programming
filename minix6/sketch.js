let t= "Draw something :)"

function setup() {
  createCanvas(windowWidth, windowHeight);
  console.log("hello world");
  frameRate(50);
  background(201, 87, 136);
}

function draw() {

  //text
    fill('white')
    strokeWeight(3)
    textSize(40);
    text(t,470,60)

  if (mouseIsPressed) {
    noStroke();
    fill(201, 87, 136);
  } else {
    noStroke();
    fill('#fae');
  }
  ellipse(mouseX, mouseY, 40, 40);
}


function windowResized() {
  resizeCanvas(windowWidth,windowHeight);
}
