**MINIX7**

[Link to my work](http://frederikkesimonsen.gitlab.io/aesthetic-programming/minix7)

[Code repository 1](https://gitlab.com/Frederikkesimonsen/aesthetic-programming/-/blob/master/minix7/sketch.js)

[Code repository 2](https://gitlab.com/Frederikkesimonsen/aesthetic-programming/-/blob/master/minix7/snake.js)

![](class_07.png)


So this week I’ve created a coronavirus game similar to the game Snake, which we all probably know. I have drawn a lot of inspiration from Daniels Schiffman’s The coding Train video (see references). I took most of the code from the p5.js link in references, and made the game into my own. I wanted to recreate the old snake game and turn it into something new that links more to today’s world and society. I therefore thought that it would be fun to make it into a coronavirus theme, because that is something that affects us all right now, and what’s more discussed these days than that? So the background of the game is an illusion of a hospital hallway, where the ‘snake’ is a monster that has to catch the virus. The more viruses you catch, the longer the tail will the monster get. The game is therefore really simple, but I find the snake game to be so iconic that I wanted this game to have the same simplicity as well.


So this week’s focus on object-oriented programming has been quite challenging, but nonetheless it has been fun to explore this side of coding. I especially found it useful the way we learned about classes, and how this can help you make the code more accessible and easier to manage, for example how I did it by splitting my code up so that the code for my ‘snake’ was all by itself. This makes it easier to both read and change, but also make the code more manageable. This separates the object and the functions, which is something that makes this challenging topic a bit easier. In object-oriented programming it’s about how you make the object look and act, so that’s why I focused quite a lot on the aesthetic and making a coherent theme, instead of just making something random I think looks pretty. So you can draw parallels to the assigned reading by looking at how they describe object oriented programming as something where _“…certain kind of unknowability is produced trough the technically facilitated processes of abstraction.”_

**References:**

**Book:** Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

**YouTube Video:** Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train: https://www.youtube.com/watch?v=l0HoJHc-63Q&list=RDCMUCvjgXvBlbQiydffZU7m1_aw&index=22 

**p5.js example:** https://p5js.org/examples/interaction-snake-game.html 

**Text:** Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge: Polity, 2017).
