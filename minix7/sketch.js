let snake;
let rez = 20;
let food;
let w;
let h;
let uImg;
let tImg;
let bImg;

function preload() {
  uImg = loadImage('virus.png');
  tImg = loadImage('monster.png');
  bImg = loadImage('backg.jpg');
}

function setup() {
  createCanvas(500,500);
  w = floor(width / rez);
  h = floor(height / rez);
  frameRate(7);
  snake = new Snake();
  foodLocation();
}

function foodLocation() {
  let x = floor(random(w));
  let y = floor(random(h));
  food = createVector(x, y);

}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    snake.setDir(-1, 0);
  } else if (keyCode === RIGHT_ARROW) {
    snake.setDir(1, 0);
  } else if (keyCode === DOWN_ARROW) {
    snake.setDir(0, 1);
  } else if (keyCode === UP_ARROW) {
    snake.setDir(0, -1);
  } else if (key == ' ') {
    snake.grow();
  }

}

function draw() {
  scale(rez);
  background(bImg);
  if (snake.eat(food)) {
    foodLocation();
  }
  snake.update();
  snake.show();


  if (snake.endGame()) {
    print("END GAME");
    background(22, 41, 24);
    noLoop();
  }

  noStroke();
  fill(48, 27, 77);
  image(uImg, food.x, food.y, 1.5, 1.5);
}
