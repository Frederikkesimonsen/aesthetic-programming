**Final minix project**

![](finalminiX.png)

**The Ideal World**

_Group 11: Mie Enemark, Cecilie Knudsen, Frederikke Breum, Frederikke Simonsen_

Click here to see our program and the project:

[The program](https://gitlab.com/frederikke.n.breum/aesthetic-programming/-/tree/master/finalminix)

[The project](https://frederikke.n.breum.gitlab.io/aesthetic-programming/finalminix/)

[The ReadMe](https://gitlab.com/Miebuch99/aestheticprogramming/-/tree/master/finalminix)


